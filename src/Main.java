import org.json.*;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;



public class Main {

    public static void main(String[] args) throws Exception {
	// write your code here
        HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();
        HttpRequest httpRequest = HttpRequest.newBuilder().GET().uri(URI.create("https://api.darksky.net/forecast/27dee9aafced685113cb32323af819a1/37.8267,-122.4233")).build();
        HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
        String body = response.body();
        System.out.println(body);
        JSONObject data = new JSONObject(body);
        System.out.println(data.toString(6));
        float longitude = data.getFloat("longitude");
        JSONObject hourly = data.getJSONObject("hourly");
        JSONArray hdata = hourly.getJSONArray("data");
        System.out.println(hdata);


    }
}
